<!--
  © 2024 Adam Blažek <blazead5@cvut.cz>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# CurveGuess

In computer graphics, cubic curves and surfaces are the most popular way to create complex smooth shapes. They come in many varieties, with Bézier curves and surfaces being the most common. A Bézier cubic curve is determined by four points in space. Two of them are its endpoints, but its relationship with the middle two is not so straightforward.

**CurveGuess** is a game where you are shown a Bézier curve with the task of determining its control points. The closer you get to the actual position, the better your score. The game also supports Catmull-Rom curves and Bézier surfaces. Since a Catmull-Rom curve interpolates its control points, they are easier to guess, so the scoring for them is stricter. On the other hand, Bézier surfaces are even more difficult – not only do you have to guess four control points instead of two, but the geometrical interpretation is even less obvious, so the scoring for them is less strict.

## Screenshots

![Screenshot](screenshots/0.png)

![Screenshot](screenshots/1.png)

## How to play

The game is written in [Nim](https://nim-lang.org/), so it requires the Nim toolchain to be installed. Download the game by cloning this repository and build it with the command:

```sh
nimble build -d:release
```

Then simply run `./curveguess`.

You will be shown a Bézier curve stretched between two pedestals. Use the **WASD** keys to look at it from all sides and distances. The two gray lines indicate the tangents on which the two middle control points always lie. Use the **↑↓** keys to guess the position of one control point, then press **→** to switch to the other one and guess it too. When you’re satisfied with your guesses, press **Space** to see the actual positions and get your score. After that, you can press **Space** to play again with a new curve.

You can also try other shapes. Press **C** to switch to Catmull-Rom curves and **F** to switch to Bézier surfaces. You can view the controls in-game at any time by pressing **H**. When you’re done playing, press **Esc** to exit the game.

## Note

This game was made by Adam Blažek (a Mathematical Informatics student) as a semestral project for a Computer Graphics course in 2024.

The [raylib](https://www.raylib.com/) library along with the [naylib](https://github.com/planetis-m/naylib) wrapper is used for rendering basic shapes in 3D. The rendering of cubic curves and surfaces is implemented in terms of the basic shapes as a part of the project.
