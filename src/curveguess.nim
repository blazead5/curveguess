# © 2024 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later

import pkg/raylib
import pkg/raymath
import std/math except E
import std/random
randomize()

func distanceSqr(v1, v2: Vector3): float32 {.inline.} =
  ## Computes the square of the Euclidean distance between two vectors.
  ## This function should be a part of Naylib, but it is not available due to a bug.
  let dx = v2.x - v1.x
  let dy = v2.y - v1.y
  let dz = v2.z - v1.z
  result = dx * dx + dy * dy + dz * dz

type
  Shape = ref object of RootObj ## A shape whose properties are to be guessed.
    guesses: seq[Vector3]
    selectedGuess: Natural

  CubicCurve = ref object of Shape
    points: array[4, Vector3]
    color: proc(t: float): Color

  BézierCurve = ref object of CubicCurve
    guessDistances: array[2, float32]

  CatmullRomCurve = ref object of CubicCurve
    guessTs: array[2, float32]

  BézierSurface = ref object of Shape
    points: array[4, array[4, Vector3]]
    color: proc(u, v: float): Color

  GameState = enum
    Guessing
    Guessed

  Game = object
    state: GameState
    showingHelp: bool
    time: float
    shapeGenerator: proc(): Shape {.nimcall.}
      ## The currently selected function for generating new shapes.
    shape: Shape
    camera: Camera3D
    cameraDistance: float = 3.0
    cameraAngle: float = 0.0

func bézierInterpolate(points: array[4, Vector3], t: float): Vector3 =
  ## Finds the position on a Bézier curve with the given control points
  ## and interpolation parameter.
  let t1 = 1.0 - t
  points[0] * (t1 * t1 * t1) + points[1] * (3 * t * t1 * t1) +
    points[2] * (3 * t * t * t1) + points[3] * (t * t * t)

method point(curve: CubicCurve, t: float): Vector3 {.base.} =
  ## Finds the position on the given cubic curve with the given
  ## interpolation parameter.
  discard

method point(curve: BézierCurve, t: float): Vector3 =
  bézierInterpolate(curve.points, t)

method point(curve: CatmullRomCurve, t: float): Vector3 =
  let i = int(t * 3)
  let t = (t * 3) - float(i)
  var p0, p1, p2, p3: Vector3
  case i
  of 0:
    p0 = curve.points[0] * 2 - curve.points[1]
    p1 = curve.points[0]
    p2 = curve.points[1]
    p3 = curve.points[2]
  of 1:
    p0 = curve.points[0]
    p1 = curve.points[1]
    p2 = curve.points[2]
    p3 = curve.points[3]
  of 2:
    p0 = curve.points[1]
    p1 = curve.points[2]
    p2 = curve.points[3]
    p3 = curve.points[3] * 2 - curve.points[2]
  else:
    return curve.points[3]
  return
    (
      p1 * 2 + (-p0 + p2) * t + (p0 * 2 - p1 * 5 + p2 * 4 - p3) * t ^ 2 +
      (-p0 + p1 * 3 - p2 * 3 + p3) * t ^ 3
    ) * 0.5

func point(surface: BézierSurface, u, v: float): Vector3 =
  ## Finds the position on the given Bézier curve with the given
  ## interpolation parameters.
  var points: array[4, Vector3]
  for index, slice in surface.points:
    points[index] = bézierInterpolate(slice, u)
  bézierInterpolate(points, v)

func updateGuess(curve: BézierCurve, index: range[0 .. 1]) =
  ## Updates the position of the given guess to match the `guessDistances` value.
  case index
  of 0:
    curve.guesses[0] =
      curve.points[0] +
      normalize(curve.points[1] - curve.points[0]) * curve.guessDistances[0]
  of 1:
    curve.guesses[1] =
      curve.points[3] +
      normalize(curve.points[2] - curve.points[3]) * curve.guessDistances[1]

func updateGuess(curve: CatmullRomCurve, index: range[0 .. 1]) =
  ## Updates the position of the given guess to match the `guessTs` value.
  curve.guesses[index] = curve.point(curve.guessTs[index])

method moveGuess(shape: Shape, amount: float32) {.base.} =
  discard

method moveGuess(curve: BézierCurve, amount: float32) =
  curve.guessDistances[curve.selectedGuess] = max(
    0.0,
    curve.guessDistances[curve.selectedGuess] +
      0.01 * amount * (-1.0) ^ curve.selectedGuess,
  )
  curve.updateGuess(curve.selectedGuess)

method moveGuess(curve: CatmullRomCurve, amount: float32) =
  curve.guessTs[curve.selectedGuess] = clamp(
    curve.guessTs[curve.selectedGuess] + 0.001 * amount,
    0.5 * curve.selectedGuess.float32 .. 0.5 + 0.5 * curve.selectedGuess.float32,
  )
  curve.updateGuess(curve.selectedGuess)

method moveGuess(surface: BézierSurface, amount: float32) =
  surface.guesses[surface.selectedGuess].z += 0.01 * amount

proc prevGuess(shape: Shape) =
  if shape.selectedGuess == shape.guesses.low:
    shape.selectedGuess = shape.guesses.high
  else:
    shape.selectedGuess.dec()

proc nextGuess(shape: Shape) =
  if shape.selectedGuess == shape.guesses.high:
    shape.selectedGuess = shape.guesses.low
  else:
    shape.selectedGuess.inc()

const curveColors = [
  proc(t: float): Color =
    Color(a: 255u8, r: uint8(256.0 * t), g: 0u8, b: 255u8)
  ,
  proc(t: float): Color =
    Color(a: 255u8, r: uint8(256.0 * t), g: 255u8, b: 0u8)
  ,
  proc(t: float): Color =
    Color(a: 255u8, r: 0u8, g: uint8(256.0 * t), b: 255u8)
  ,
  proc(t: float): Color =
    Color(a: 255u8, r: 255u8, g: uint8(256.0 * t), b: 0u8)
  ,
  proc(t: float): Color =
    Color(a: 255u8, r: 0u8, g: 255u8, b: uint8(256.0 * t))
  ,
  proc(t: float): Color =
    Color(a: 255u8, r: 255u8, g: 0u8, b: uint8(256.0 * t))
  ,
] ## Functions used for coloring curves.

const colorMax = 255.999
const surfaceColors = [
  proc(u, v: float): Color =
    Color(a: 255u8, r: uint8(colorMax * u), g: uint8(colorMax * v), b: 255u8)
  ,
  proc(u, v: float): Color =
    Color(a: 255u8, r: uint8(colorMax * u), g: 255u8, b: uint8(colorMax * v))
  ,
  proc(u, v: float): Color =
    Color(a: 255u8, r: uint8(colorMax * v), g: uint8(colorMax * u), b: 255u8)
  ,
  proc(u, v: float): Color =
    Color(a: 255u8, r: 255u8, g: uint8(colorMax * u), b: uint8(colorMax * v))
  ,
  proc(u, v: float): Color =
    Color(a: 255u8, r: uint8(colorMax * v), g: 255u8, b: uint8(colorMax * u))
  ,
  proc(u, v: float): Color =
    Color(a: 255u8, r: 255u8, g: uint8(colorMax * v), b: uint8(colorMax * u))
  ,
] ## Functions used for coloring surfaces.

proc randomBézierCurve(): Shape =
  const xzRange = -2.0f32 .. 2.0f32
  const yRange = 0.1f32 .. 0.9f32
  let curve = BézierCurve(
    points: [
      Vector3(x: 0.0, y: -1.0, z: 0.0),
      Vector3(x: rand(xzRange), y: -rand(yRange), z: rand(xzRange)),
      Vector3(x: rand(xzRange), y: rand(yRange), z: rand(xzRange)),
      Vector3(x: 0.0, y: 1.0, z: 0.0),
    ],
    guesses: newSeq[Vector3](2),
    guessDistances: [1.5f32, 1.5f32],
    color: curveColors.sample(),
  )
  for index in 0 .. 1:
    curve.updateGuess(index)
  curve

proc randomCatmullRomCurve(): Shape =
  const xzRange = -0.7f32 .. 0.7f32
  const yRange = 0.1f32 .. 0.8f32
  const guessDelta = 0.05f32
  const third = 1.0f32 / 3.0f32
  let curve = CatmullRomCurve(
    points: [
      Vector3(x: 0.0, y: -1.0, z: 0.0),
      Vector3(x: rand(xzRange), y: -rand(yRange), z: rand(xzRange)),
      Vector3(x: rand(xzRange), y: rand(yRange), z: rand(xzRange)),
      Vector3(x: 0.0, y: 1.0, z: 0.0),
    ],
    guesses: newSeq[Vector3](2),
    guessTs: [
      rand(third - guessDelta .. third + guessDelta),
      rand(2 * third - guessDelta .. 2 * third + guessDelta),
    ],
    color: curveColors.sample(),
  )
  for index in 0 .. 1:
    curve.updateGuess(index)
  curve

proc randomBézierSurface(): Shape =
  const zRange = -0.9f32 .. 0.9f32
  const third = 1.0f32 / 3.0f32
  let surface = BézierSurface(
    color: surfaceColors.sample(),
    guesses:
      @[
        Vector3(x: -third, y: -third, z: 0.0),
        Vector3(x: -third, y: third, z: 0.0),
        Vector3(x: third, y: -third, z: 0.0),
        Vector3(x: third, y: third, z: 0.0),
      ],
  )
  for i, slice in surface.points.mpairs:
    for j, point in slice.mpairs:
      point = Vector3(x: i.float / 1.5 - 1.0, y: j.float / 1.5 - 1.0, z: rand(zRange))
  surface

proc start(game: var Game) =
  game.state = Guessing
  game.shape = game.shapeGenerator()

proc nextState(game: var Game) =
  case game.state
  of Guessing:
    game.state = Guessed
  of Guessed:
    game.start()

method score(shape: Shape): Natural {.base.} =
  ## Calculates the score for guessing the control points of
  ## the given shape.
  discard

method score(curve: BézierCurve): Natural =
  let dist0 = distanceSqr(curve.guesses[0], curve.points[1])
  let dist1 = distanceSqr(curve.guesses[1], curve.points[2])
  return Natural((dist0 + dist1) * 10000)

method score(curve: CatmullRomCurve): Natural =
  const third = 1.0f32 / 3.0f32
  return Natural(
    ((curve.guessTs[0] - third) ^ 2 + (curve.guessTs[1] - 2 * third) ^ 2) * 1000000
  )

method score(surface: BézierSurface): Natural =
  let dist0 = distanceSqr(surface.guesses[0], surface.points[1][1])
  let dist1 = distanceSqr(surface.guesses[1], surface.points[1][2])
  let dist2 = distanceSqr(surface.guesses[2], surface.points[2][1])
  let dist3 = distanceSqr(surface.guesses[3], surface.points[2][2])
  return Natural((dist0 + dist1 + dist2 + dist3) * 2000)

method draw(shape: Shape, time: float) {.base.} =
  discard

method draw(curve: CubicCurve, time: float) =
  const numSegments = 128
  var lastPoint = curve.points[0]
  for i in 1 .. numSegments:
    let t = i.float / numSegments.float
    let point = curve.point(t)
    drawCapsule(
      lastPoint, point, 0.01, 1, 1, curve.color(abs((2 * t + time) mod 2.0 - 1))
    )
    lastPoint = point

method draw(surface: BézierSurface, time: float) =
  const numSegments = 64
  func f(t: float): float =
    abs((2 * t + time) mod 2.0 - 1)
  for i in 0 ..< numSegments:
    let u = i.float / numSegments.float
    let u1 = i.succ.float / numSegments.float
    for j in 0 ..< numSegments:
      let v = j.float / numSegments.float
      let v1 = j.succ.float / numSegments.float
      let p00 = surface.point(u, v)
      let p01 = surface.point(u, v1)
      let p10 = surface.point(u1, v)
      let p11 = surface.point(u1, v1)
      drawTriangle3D(p00, p01, p11, surface.color(f(u), f(v1)))
      drawTriangle3D(p11, p01, p00, surface.color(f(u), f(v1)))
      drawTriangle3D(p00, p10, p11, surface.color(f(u1), f(v)))
      drawTriangle3D(p11, p10, p00, surface.color(f(u1), f(v)))

method drawPoints(shape: Shape) {.base.} =
  ## Draws the control points of the given shape.
  discard

method drawPoints(curve: CubicCurve) =
  const radius = 0.03
  for point in curve.points:
    drawSphere(point, radius, Color(a: 0xFF, r: 0x00, g: 0xBB, b: 0x00))

method drawPoints(surface: BézierSurface) =
  const radius = 0.03
  for slice in surface.points:
    for point in slice:
      drawSphere(point, radius, Color(a: 0xFF, r: 0x00, g: 0xBB, b: 0x00))

proc drawGuessPoints(shape: Shape, time: float, highlightSelected: bool) =
  const radius = 0.03
  const selectedRadius = proc(time: float): float =
    0.025 + 0.005 * cos(time * Tau)
  const color = Color(a: 255, r: 128, g: 0, b: 0)
  const selectedColor = proc(time: float): Color =
    Color(a: 255, r: 255, g: uint8(128.0 - 127.0 * cos(time * Tau)), b: 0)
  for index, guess in shape.guesses:
    if highlightSelected and index == shape.selectedGuess:
      drawSphere(guess, selectedRadius(time), selectedColor(time))
    else:
      drawSphere(guess, radius, color)

method drawGuesses(shape: Shape, time: float, highlightSelected: bool) {.base.} =
  discard

method drawGuesses(curve: BézierCurve, time: float, highlightSelected: bool) =
  block handles:
    const color = Color(a: 255, r: 64, g: 64, b: 64)
    const selectedColor = Color(a: 255, r: 128, g: 128, b: 128)
    drawRay(
      Ray(position: curve.points[0], direction: curve.points[1] - curve.points[0]),
      if highlightSelected and curve.selectedGuess == 0: selectedColor else: color,
    )
    drawRay(
      Ray(position: curve.points[3], direction: curve.points[2] - curve.points[3]),
      if highlightSelected and curve.selectedGuess == 1: selectedColor else: color,
    )
  block points:
    curve.drawGuessPoints(time, highlightSelected)

method drawGuesses(curve: CatmullRomCurve, time: float, highlightSelected: bool) =
  curve.drawGuessPoints(time, highlightSelected)

method drawGuesses(surface: BézierSurface, time: float, highlightSelected: bool) =
  block handles:
    const color = Color(a: 255, r: 64, g: 64, b: 64)
    const selectedColor = Color(a: 255, r: 128, g: 128, b: 128)
    for i, slice in surface.points[1 .. 2]:
      for j, point in slice[1 .. 2]:
        for direction in [-1.0f32, 1.0f32]:
          drawRay(
            Ray(position: point, direction: Vector3(x: 0.0, y: 0.0, z: direction)),
            if highlightSelected and surface.selectedGuess == 2 * i + j:
              selectedColor
            else:
              color
            ,
          )
  block points:
    surface.drawGuessPoints(time, highlightSelected)

proc drawPedestals() =
  const radius = 1.0
  const height = 0.1
  const sides = 8
  const color = Color(a: 0xFF, r: 0x55, g: 0x55, b: 0x55)
  drawCylinder(
    Vector3(x: 0.0, y: -1 - height, z: 0.0), radius, radius, height, sides, color
  )
  drawCylinderWires(
    Vector3(x: 0.0, y: -1 - height, z: 0.0), radius, radius, height, sides, Black
  )
  drawCylinder(Vector3(x: 0.0, y: 1, z: 0.0), radius, radius, height, sides, color)
  drawCylinderWires(Vector3(x: 0.0, y: 1, z: 0.0), radius, radius, height, sides, Black)

proc drawScore(score: Natural) =
  drawText(cstring("Score (lower is better) "), 5, 5, 20, White)
  drawText(cstring($score), 5, 30, 40, White)

proc drawHelp() =
  const margin = 60'i32
  const border = 5'i32
  drawRectangle(
    posX = margin - border,
    posY = margin - border,
    width = getScreenWidth() - 2 * (margin - border),
    height = getScreenHeight() - 2 * (margin - border),
    color = Color(a: 0xFF, r: 0xFF, g: 0xFF, b: 0xFF),
  )
  drawRectangle(
    posX = margin,
    posY = margin,
    width = getScreenWidth() - 2 * margin,
    height = getScreenHeight() - 2 * margin,
    color = Color(a: 0xFF, r: 0x33, g: 0x33, b: 0x33),
  )
  const lines = [
    "Try to guess the control points of cubic curves and surfaces!", "", "Esc: quit",
    "H: toggle help", "WASD: move camera", "Up/Down: move guess",
    "Left/Right: toggle guess", "Space: confirm guesses / try again",
    "B: switch to Bézier curves", "C: switch to Catmull-Rom curves",
    "F: switch to Bézier surfaces",
  ]
  const textX = margin + 10'i32
  const textY = margin + 10'i32
  const textSize = 25'i32
  const lineHeight = textSize + 5'i32
  for index, line in lines:
    drawText(line.cstring, textX, textY + index * lineHeight, textSize, White)

proc drawHelpHelp() =
  ## Draws the help for how to show the help.
  drawText(cstring("Press H for help"), 5, 5, 20, Gray)

proc draw(game: Game) =
  drawing:
    clearBackground(Black)
    mode3D(game.camera):
      drawPedestals()
      game.shape.draw(game.time)
      game.shape.drawGuesses(game.time, highlightSelected = game.state == Guessing)
      if game.state == Guessed:
        game.shape.drawPoints()
    if game.state == Guessed:
      drawScore(game.shape.score())
    if game.showingHelp:
      drawHelp()
    elif game.state != Guessed:
      drawHelpHelp()

proc updateCamera(game: var Game) =
  game.camera.position = Vector3(
    x: game.cameraDistance * cos(game.cameraAngle),
    z: game.cameraDistance * sin(game.cameraAngle),
    y: 0.0,
  )

proc processControls(game: var Game) =
  if game.showingHelp:
    if isKeyPressed(H) or isKeyPressed(Space):
      game.showingHelp = false
  else:
    if isKeyPressed(Space):
      game.nextState()
    if isKeyDown(A):
      game.cameraAngle += Pi / 60.0
    if isKeyDown(D):
      game.cameraAngle -= Pi / 60.0
    if isKeyDown(W) and game.cameraDistance > 1.6:
      game.cameraDistance -= 0.1
    if isKeyDown(S):
      game.cameraDistance += 0.1
    if isKeyPressed(Left):
      game.shape.prevGuess()
    if isKeyPressed(Right):
      game.shape.nextGuess()
    if isKeyDown(Up) and game.state == Guessing:
      game.shape.moveGuess(1)
    if isKeyDown(Down) and game.state == Guessing:
      game.shape.moveGuess(-1)
    if isKeyPressed(H):
      game.showingHelp = true
    if isKeyPressed(B):
      game.shapeGenerator = randomBézierCurve
      game.start()
    if isKeyPressed(C):
      game.shapeGenerator = randomCatmullRomCurve
      game.start()
    if isKeyPressed(F):
      game.shapeGenerator = randomBézierSurface
      game.start()

proc main() =
  const fps = 30
  var game = Game(
    shapeGenerator: randomBézierCurve,
    camera: Camera3D(
      target: Vector3(x: 0.0, y: 0.0, z: 0.0),
      up: Vector3(x: 0.0, y: 1.0, z: 0.0),
      fovy: 60.0,
      projection: Perspective,
    ),
  )
  game.start()
  setConfigFlags(flags(VsyncHint, WindowResizable))
  setTargetFPS(fps)
  initWindow(1280, 960, "CurveGuess")
  while not windowShouldClose():
    game.processControls()
    game.updateCamera()
    game.draw()
    game.time += 1.0 / fps

main()
