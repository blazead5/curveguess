# © 2024 Adam Blažek <blazead5@cvut.cz>
# SPDX-License-Identifier: GPL-3.0-or-later

# Package

version = "2024.701.0"
author = "Adam Blažek"
description = "A game about guessing the control points of curves"
license = "GPL-3.0-or-later"
srcDir = "src"
bin = @["curveguess"]

# Dependencies

requires "nim >= 2.0.2"

requires "naylib"
